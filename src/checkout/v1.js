const basket = {
  customer: {
    id: '0001',
    name: 'Alice',
    vat: '19%',
  },
  items: [
    {
      product: { id: '0001', name: 'Granit Schotter', price: 500 },
      qty: 10
    },
    {
      product: { id: '0002', name: 'Quarzsand', price: 690 },
      qty: 15
    },
    {
      product: { id: '0003', name: 'Rindenmulch', price: 250 },
      qty: 20
    },
    {
      product: { id: '0004', name: 'Betonkies', price: 350 },
      qty: 0
    },
    {
      product: { id: '0005', name: 'Basalt', price: 720 },
      qty: 25
    },
  ]
};

// expected:
// lineItems == [
//   { id: '0001', name: 'Granit Schotter', total: '50,00 €' },
//   { id: '0002', name: 'Quarzsand', total: '103,50 €' },
//   { id: '0003', name: 'Rindenmulch', total: '50,00 €' },
//   { id: '0005', name: 'Basalt', total: '180,00 €' }
// ]
const lineItems = []; // @TODO: complete me!

// expected:
// invoice == {
//   lineItems,
//   subtotal: '383,50 €',
//   vat: '72,87 €',
//   overall: '456,37 €'
// }
export const invoice = {
  lineItems,
  // @TODO: complete me!
};