import prompts from 'prompts';
import Table from 'cli-table';
import 'colors';

const padLeft = (text, len = 9) => {
  if (text.length >= len) {
    return text;
  }

  let pad = '';
  for (let i = 0; i < len-text.length; i++) {
    pad += ' ';
  }

  return `${pad}${text}`;
}

const asTableRows = invoice => {
  if (!invoice.lineItems.length) {
    return [];
  }

  const lineItems = invoice.lineItems.map(item => [
    item.id, 
    item.name, 
    padLeft(item.total)
  ]);
  return [
    ...lineItems,
    ['', 'Subtotal'.bold, padLeft(invoice.subtotal).bold],
    ['', 'VAT'.bold, padLeft(invoice.vat).bold],
    ['', 'Overall'.bold.underline, padLeft(invoice.overall).bold.underline],
  ];
};

(async () => {
  const response = await prompts([
    {
      type: 'select',
      name: 'version',
      message: 'Which checkout version do you want to use?',
      choices: [
        { title: 'Version 1', value: 'v1' },
        { title: 'Version 2', value: 'v2' },
      ]
    },
  ]);

  const { invoice } = await import(`./src/checkout/${response.version}.js`);

  const table = new Table({
    head: ['ID', 'Product', 'Price'],
  });
  
  const rows = asTableRows(typeof(invoice) == 'function' ? await invoice() : invoice);
  if (!rows.length) {
    return console.error('[Error] No line items. Aborting.'.bold.red);
  }
  
  table.push(...rows);
  console.log(table.toString());
})();