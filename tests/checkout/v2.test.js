import { expect, describe, it } from '@jest/globals';
import { invoice } from '../../src/checkout/v2';

describe('invoice', () => {
  describe('valid order', () => {
    it('should run correctly', async () => {
      const expectedLineItems = [
        { id: '0001', name: 'Granit Schotter', total: '50,00 €' },
        { id: '0002', name: 'Quarzsand', total: '103,50 €' },
        { id: '0003', name: 'Rindenmulch', total: '50,00 €' },
        { id: '0005', name: 'Basalt', total: '180,00 €' }
      ];

      const inv = await invoice();
      expect(inv.lineItems).toStrictEqual(expectedLineItems);
      expect(inv.subtotal).toEqual('383,50 €');
      expect(inv.vat).toEqual('72,87 €');
      expect(inv.overall).toEqual('456,37 €');
    });
  }, 3000);
});